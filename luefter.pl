#!/usr/bin/perl
use strict;
use warnings;
use diagnostics;

use Tie::IxHash;

`sudo "/usr/bin/jetson_clocks"`;

tie my %geschwindigkeit_temperatur, "Tie::IxHash";

%geschwindigkeit_temperatur = (
    45 => 0,
    46 => 120,
    47 => 170,
    48 => 210,
    49 => 230,
    50 => 255,
);

while ( 1 ){
    while( my ( $temperatur, $geschwindigkeit ) = each %geschwindigkeit_temperatur ){
	my $temperatur = `cat /sys/class/thermal/thermal_zone0/temp`;
	$temperatur = sprintf "%2d\n", $temperatur / 1000;
	my $pwm = $geschwindigkeit_temperatur{ $temperatur };
	my $aktuell = `cat /sys/devices/pwm-fan/target_pwm`;
	# print STDERR sprintf "\33[K%d Grad zugeordnete Lüftergeschwindigkeit: %d, tatsächlich: %d\r", $temperatur, $pwm, $aktuell;
	sleep 1;
	`sudo echo $pwm > /sys/devices/pwm-fan/target_pwm`;
    }
}
